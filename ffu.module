<?php
/**
 * @file
 */


/**
 * Implements hook_form_FORM_ID_alter().
 */
function ffu_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  $instance = $form['#instance'];

  $allowed_types = array('text_with_summary');

  if (in_array($form['#field']['type'], $allowed_types)) {
    $form['instance']['settings']['ffu'] = array(
      '#type' => 'fieldset',
      '#title' => t('FFU settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['instance']['settings']['ffu']['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable FFU for this field.'),
      '#default_value' => isset($instance['settings']['ffu']['enabled']) ? $instance['settings']['ffu']['enabled'] : 0,
    );
  }
}

/**
 * Implements hook_field_attach_insert().
 */
function ffu_field_attach_insert($entity_type, $entity) {
  ffu_field_attach_update($entity_type, $entity);
}

/**
 * Implements hook_field_attach_update().
 */
function ffu_field_attach_update($entity_type, $entity) {
  list($entity_id,, $bundle) = entity_extract_ids($entity_type, $entity);

  $instances = field_info_instances($entity_type, $bundle);
  foreach ($instances as $instance) {
    if (empty($instance['settings']['ffu']['enabled'])) {
      continue;
    }

    $langcode = field_language($entity_type, $entity, $instance['field_name']);

    $after = _ffu_process_entity($entity, $instance['field_name'], $langcode);

    if (!empty($entity->original)) {
      $before = _ffu_process_entity($entity->original, $instance['field_name'], $langcode);
    }
    else {
      $before = array();
    }

    $file = new stdClass();

    foreach (array_diff($after, $before) as $fid) {
      $file->fid = $fid;
      file_usage_add($file, 'ffu', $entity_type, $entity_id);
    }
    foreach (array_diff($before, $after) as $fid) {
      $file->fid = $fid;
      file_usage_delete($file, 'ffu', $entity_type, $entity_id);
    }
  }
}

/**
 *
 */
function _ffu_process_entity($entity, $field_name, $langcode) {
  $files = array();
  $fields = isset($entity->{$field_name}[$langcode]) ? $entity->{$field_name}[$langcode] : array();
  foreach ($fields as $field) {
    $matches = array();
    preg_match_all('~(href|src|action|longdesc)="([^"?!#]+)~i', $field['value'], $matches, PREG_SET_ORDER);
    foreach ($matches as $match) {
      $files[] = _ffu_process_matches($match);
    }
  }

  return array_filter($files);
}

/**
 *
 */
function _ffu_process_matches($matches) {
  global $base_url;

  $url = htmlspecialchars_decode($matches[2]);
  $parts = parse_url(trim($url, '/'));
  if (!isset($parts['scheme']) || !isset($parts['host'])) {
    $url = $base_url . '/' . $parts['path'];
  }

  $parts = parse_url($url);
  $info = _ffu_parse_url($url, $parts);

  if ($info['target'] !== 'internal' || empty($info['system_path'])) {
    return FALSE;
  }

  $item = menu_get_item($info['system_path']);
  $uri = NULL;

  // Private file.
  if ($item['path'] === 'system/files') {
    $args = $item['page_arguments'];

    $scheme = array_shift($args);
    $target = implode('/', $args);
    $uri = $scheme . '://' . $target;

  }
  // Private image style.
  elseif ($item['path'] === 'system/files/styles/%') {
    $args = $item['page_arguments'];

    array_shift($args);

    $scheme = array_shift($args);
    $target = implode('/', $args);
    $uri = $scheme . '://' . $target;
  }
  // File entity download.
  elseif ($item['path'] === 'file/%/download') {
    return $item['page_arguments'][0]->fid;
  }
  else {
    return;
  }

  if (empty($uri)) {
    return FALSE;
  }

  $files = file_load_multiple(array(), array('uri' => $uri));
  if (count($files)) {
    foreach ($files as $item) {
      // Since some database servers sometimes use a case-insensitive comparison
      // by default, double check that the filename is an exact match.
      if ($item->uri === $uri) {
        $file = $item;
        break;
      }
    }
  }

  if (!isset($file)) {
    return FALSE;
  }

  return $file->fid;
}

/**
 * Copy of linkit_parse_url() function.
 *
 * @see linkit_parse_url().
 */
function _ffu_parse_url($url, $parts) {
  global $base_url;

  // Make a new array, this will hold the components from parse_url() and our
  // own "Linkit" components.
  $path_info = array();

  // Append the original components from parse_url() to our array.
  $path_info += $parts;

  // Save the whole URL.
  $path_info['url'] = $url;

  if (!isset($path_info['query'])) {
    $path_info['query'] = '';
  }

  // Convert the query string to an array as Drupal can only handle querys as
  // arrays.
  // @see http://api.drupal.org/drupal_http_build_query
  parse_str($path_info['query'], $path_info['query']);

  // The 'q' parameter contains the path of the current page if clean URLs are
  // disabled. It overrides the 'path' of the URL when present, even if clean
  // URLs are enabled, due to how Apache rewriting rules work.
  if (isset($path_info['query']['q'])) {
    $path_info['path'] = $path_info['query']['q'];
    unset($path_info['query']['q']);
  }

  // Load all local stream wrappers. The $path_info['scheme'] will be tested
  // against this later to ensure it is local.
  $local_stream_wrappers = file_get_stream_wrappers(STREAM_WRAPPERS_LOCAL);

  // Internal URL.
  // We can not use the url_is_external() as it treats all absolute links as
  // external and treats all stream wrappers as internal.
  $local_url = trim($path_info['scheme'] . '://' . $path_info['host'] . base_path(), '/') == $base_url;
  $local_stream_wrapper = isset($local_stream_wrappers[$path_info['scheme']]);
  //@TODO: maybe file_valid_uri() ?

  if ($local_url || $local_stream_wrapper) {
    // Set target as internal.
    $path_info['target'] = 'internal';

    // If this is seems to be a valid local stream wrapper string, force the
    // $path_info['path'] to be set to the file_url.
    if ($local_stream_wrapper) {
      $path_info = array_merge($path_info, parse_url(file_create_url($path_info['url'])));
    }

    // Trim the path from slashes.
    $path_info['path'] = trim($path_info['path'], '/');

    // If we have an empty path, and an internal target, we can assume that the
    // URL should go the the frontpage.
    if (empty($path_info['path'])) {
      $path_info['frontpage'] = TRUE;
      $path_info['path'] = variable_get('site_frontpage', 'node');
    }

    // Try converting the path to an internal Drupal path.
    $internal_url = drupal_get_normal_path($path_info['path']);

    // Add the "real" system path (not the alias) if the current user have
    // access to the URL.
    $path_info['system_path'] = drupal_valid_path($internal_url) ? $internal_url : FALSE;

    $menu_item = menu_get_item($path_info['system_path']);
    if ($menu_item) {
      $path_info['menu']['path'] = $path_info['system_path'];
      $path_info['menu']['description'] = check_plain($menu_item['description']);
      $path_info['menu']['title'] = check_plain($menu_item['title']);
    }
    // If we have a valid stream wrapper URL, find out the internal url.
    if ($local_stream_wrapper) {
      $path_info['system_path'] = $path_info['path'];
      $path_info['menu']['path'] = $path_info['path'];
      $path_info['menu']['description'] = 'This funciton is not fully integrated yet.';
      $path_info['menu']['title'] = 'This funciton is not fully integrated yet.';
    }
  }
  else {
    // Set target as external.
    $path_info['target'] = 'external';
  }

  return $path_info;
}
